﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace DB_editor
{
    public partial class Form1 : Form
    {
        DataView dw1;               //отображает все проекты        
        Point point = new Point();  //хранит номер столбца и строки ячейки DataGridView1, находящейся в данный момент под курсором мышки

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (DAL.LoadData() == false)    //заполняем таблицы DataSet данными из БД
                this.Close();

            dw1 = new DataView(DAL.myDataSet.Tables["maindata"]);
            dataGridView1.DataSource = dw1;

            this.dataGridView1.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);

            FillComboboxes();	//заполняем комбобоксы

            this.textBox25.TextChanged += new System.EventHandler(this.Filter_TextChanged);     //подписываемся на события TextChanged
            this.comboBox21.TextChanged += new System.EventHandler(this.Filter_TextChanged);    //только после метода FillComboboxes()
            this.comboBox22.TextChanged += new System.EventHandler(this.Filter_TextChanged);
            this.comboBox23.TextChanged += new System.EventHandler(this.Filter_TextChanged);
            this.comboBox1.TextChanged += new System.EventHandler(this.Filter_TextChanged);
            this.comboBox2.TextChanged += new System.EventHandler(this.Filter_TextChanged);
            
            //настраиваем видимость столбцов:
            foreach (DataGridViewColumn dataColumn in dataGridView1.Columns)    //пробегаемся по всем столбцам
            {
                try
                {
                    dataColumn.Visible = Convert.ToBoolean(Properties.Settings.Default[dataColumn.Name]);
                }
                catch
                {
                    // Ничего не делаем
                    // MessageBox.Show(ex.Message);
                }                
            }
            dataGridView1.Columns["id_organization"].Visible = Properties.Settings.Default.showIDColumns;
            dataGridView1.Columns["id_status"].Visible = Properties.Settings.Default.showIDColumns;
            dataGridView1.Columns["id_label1"].Visible = Properties.Settings.Default.showIDColumns;
            dataGridView1.Columns["id_label2"].Visible = Properties.Settings.Default.showIDColumns;
            dataGridView1.Columns["id_director"].Visible = Properties.Settings.Default.showIDColumns;
            dataGridView1.Columns["id_normoControl"].Visible = Properties.Settings.Default.showIDColumns;
            dataGridView1.Columns["id_engineer"].Visible = Properties.Settings.Default.showIDColumns;
            dataGridView1.Columns["id_executor"].Visible = Properties.Settings.Default.showIDColumns;

            //отображаем русские заголовки столбцов, пользуясь columnDictionary
            foreach (DataGridViewColumn dataColumn in dataGridView1.Columns)    //пробегаемся по всем столбцам
            {
                if (Translate.columnsDictionary.ContainsKey(dataColumn.Name)) //если такой заголовок есть в словаре
                    dataColumn.HeaderText = Translate.columnsDictionary[dataColumn.Name]; //отображаем русский текст
            }

            //прочие красивости:
			this.Text = "Редактор базы проектов" + "  " + Application.ProductVersion;
            dataGridView1.Columns["CreationYear"].Width = 40;
            dataGridView1.Columns["ProjectNumber"].DefaultCellStyle.BackColor = Color.Azure;
            dataGridView1.Columns["AdditionalNumber"].Width = 50;
            dataGridView1.Columns["FullObjectName"].Width = 120;
            dataGridView1.Columns["DesignWorksStage"].Width = 50;
            dataGridView1.Columns["ProjectReadiness"].DefaultCellStyle.BackColor = Color.Azure;
            
            dataGridView1.RowHeadersWidth = 4;
            dataGridView1.ColumnHeadersHeight = 47;
            dataGridView1.ContextMenuStrip = contextMenuStrip1;

            DAL.projectnumber = DAL.myDataSet.Tables["maindata"].Rows[0]["ProjectNumber"].ToString();

            dataGridView1.Select();
            dataGridView1.Rows[0].Selected = true;
            label27.Text = "Показано проектов: " + dataGridView1.Rows.Count.ToString();

            comboBox23.MaxDropDownItems = 40;
        }

        private void FilterData()
        {
            dw1.RowFilter = string.Format("{0}{4}{1}{2}{5}("
                + "[organization] LIKE '*{3}*' "
                + "OR [numberTU] LIKE '*{3}*' "
                + "OR [otherNumber] LIKE '*{3}*' "
                + "OR [projectnumber] LIKE '*{3}*' "
                + "OR [additionalNumber] LIKE '*{3}*' "
                + "OR [fullObjectName] LIKE '*{3}*' "
                + "OR [shortObjectName] LIKE '*{3}*' "
                + "OR [status] LIKE '*{3}*' "
                + "OR [label1] LIKE '*{3}*' "
                + "OR [label2] LIKE '*{3}*' "
                + "OR [projectSection] LIKE '*{3}*' "
                + "OR [designWorksStage] LIKE '*{3}*' "
                + "OR [director] LIKE '*{3}*' "
                + "OR [normoControl] LIKE '*{3}*' "
                + "OR [engineer] LIKE '*{3}*' "
                + "OR [executor] LIKE '*{3}*' "
                + "OR [initialDataGathering] LIKE '*{3}*' "
                + "OR [geodesyTechnicalReport] LIKE '*{3}*' "
                + "OR [geodesyDeliveryTasks] LIKE '*{3}*' "
                + "OR [projectReadiness] LIKE '*{3}*' "
                + "OR [documentationDelivery] LIKE '*{3}*' "

                + "OR [RTNadmittance] LIKE '*{3}*' "
                + "OR [landPlotSelection] LIKE '*{3}*' "
                + "OR [GEOissuance] LIKE '*{3}*' "
                + "OR [technicalReport] LIKE '*{3}*' "

                + "OR [note] LIKE '*{3}*')",
                qweryConstructor1("creationYear", comboBox21.SelectedItem.ToString()),
                qweryConstructor2("label1", comboBox22.SelectedItem.ToString()),
                qweryConstructor2("label2", comboBox23.SelectedItem.ToString()),
                textBox25.Text,
                qweryConstructor2("designWorksStage", comboBox1.SelectedItem.ToString()),
                qweryConstructor2("executor", comboBox2.SelectedItem.ToString()));
            
            label27.Text = "Показано проектов: " + dataGridView1.Rows.Count.ToString();
        }

        private string qweryConstructor1(string name, string selectedItem)
        {
            if (selectedItem == "ВСЕ")
                return "";

            return "([" + name + "] = " + selectedItem + ") AND";
        }

        private string qweryConstructor2(string name, string selectedItem)
        {
            if (selectedItem == "ВСЕ")
                return "";

            return "([" + name + "] = '" + selectedItem + "') AND";
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DAL.projectnumber = dataGridView1.Rows[e.RowIndex].Cells["projectNumber"].Value.ToString();
        }

        private void Filter_TextChanged(object sender, EventArgs e) //на это событие подписаны все элементы из фильтров
        {
            FilterData();
        }

        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.Select();
        }

        # region Обработчики для кнопок //================================================================================================
        
        private void button2_Click(object sender, EventArgs e)  //Сбросить фильтры
        {
            DropFilters();
        }
        # endregion Обработчики для кнопок //=============================================================================================

        private void EditingMode()  //настройка элементов формы в режиме правки одного проекта
        {
            Form2 f2 = new Form2();
            f2.ShowDialog(this);
        }

        private void FillComboboxes()   //заполнить комбобоксы всеми возможными значениями из DataSet
        {
            //Год:
            comboBox21.Items.Clear();   
            comboBox21.Items.Add("ВСЕ");
            comboBox21.SelectedIndex = 0;
            //Ярлык 1:
            comboBox22.Items.Clear();
            comboBox22.Items.Add("ВСЕ");
            comboBox22.SelectedIndex = 0;
            //Ярлык 2:
            comboBox23.Items.Clear();
            comboBox23.Items.Add("ВСЕ");
            comboBox23.SelectedIndex = 0;
            //Стадия проектных работ ("Р","П" или "любая"):
            comboBox1.Items.Clear();
            comboBox1.Items.Add("ВСЕ");
            comboBox1.Items.Add("Р");
            comboBox1.Items.Add("П");
            comboBox1.SelectedIndex = 0;
            //Исполнитель:
            comboBox2.Items.Clear();   
            comboBox2.Items.Add("ВСЕ");
            comboBox2.SelectedIndex = 0;

            for (int i = 2003; i <= System.DateTime.Now.Year + 1; i++)
            {
                comboBox21.Items.Add(i.ToString());
            }

            foreach (DataRow dr in DAL.myDataSet.Tables["labels1"].Rows)
            {
                comboBox22.Items.Add(dr["label1"].ToString());
            }

            foreach (DataRow dr in DAL.myDataSet.Tables["labels2"].Rows)
            {
                comboBox23.Items.Add(dr["label2"].ToString());
            }

            foreach (DataRow dr in DAL.myDataSet.Tables["employees"].Rows)
            {
                comboBox2.Items.Add(dr["surname"].ToString());
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)   //Форма закрывается - сохраняем настройки
        {
            foreach (DataGridViewColumn dataColumn in dataGridView1.Columns)    //пробегаемся по всем столбцам dataGridView
            {
                try
                {  //сохраняем видимость столбцов dataGridView в настройки пользователя
                    Properties.Settings.Default[dataColumn.Name] = dataColumn.Visible;
                }
                catch
                {
                    //MessageBox.Show(ex.Message);
                }
                Properties.Settings.Default.Save(); //сохраняем настройки
            }
        }

        private void DeleteProject()    //Удалить проект
        {
            Form_Pass f2 = new Form_Pass();
            if (f2.ShowDialog(this) == DialogResult.OK)
            {
                DAL.DeleteProject();    //удаляем проект из базы
                DAL.LoadData();         //заново загружаем все данные
                dw1 = new DataView(DAL.myDataSet.Tables["maindata"]);
                dataGridView1.DataSource = dw1;

                FilterData();   //фильтруем обновленные данные

                dataGridView1.Select();
                dataGridView1.ClearSelection();
                f2.Dispose();
            }
        }

        private void ChangeProjectNumber()  //Сменить номер проекта
        {
            Form_NewNumber f3 = new Form_NewNumber();
            f3.ShowDialog(this);
            if (f3.DialogResult == DialogResult.OK)
            {
                int i = dataGridView1.SelectedRows[0].Index;    //запоминаем текущую позицию удаляемой строки в dataGridView1
                
                DAL.LoadData();
                dw1 = new DataView(DAL.myDataSet.Tables["maindata"]);
                dataGridView1.DataSource = dw1;

                FilterData();   //фильтруем обновленные данные

                dataGridView1.Select();
                dataGridView1.ClearSelection();
                dataGridView1.Rows[i].Selected = true;  //выделяем строку
                DAL.projectnumber = dataGridView1.Rows[i].Cells["projectNumber"].Value.ToString();
                MessageBox.Show(DAL.projectnumber);
            }
        }

        private void NewProject()   //Создать новый проект
        {
            Form_NewProject form = new Form_NewProject();
            form.ShowDialog(this);

            if (form.DialogResult == DialogResult.OK)   //ести нажали ОК и удачно прошли все проверки
            {
                DAL.LoadData();         //заново загружаем все данные
                dw1 = new DataView(DAL.myDataSet.Tables["maindata"]);
                dataGridView1.DataSource = dw1;
				
                dataGridView1.Select();
                dataGridView1.ClearSelection();
                
                textBox25.Text = DAL.projectnumber = form.TextBox1.Text.Trim();
                FilterData();   //фильтруем обновленные данные
                
                EditingMode();
            }
        }

        private void DropFilters()  //Сбросить фильтры
        {
            dw1.RowFilter = "";
            comboBox21.SelectedIndex = 0;
            comboBox22.SelectedIndex = 0;
            comboBox23.SelectedIndex = 0;
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            textBox25.Text = "";
        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            point.X = e.ColumnIndex;
            point.Y = e.RowIndex;
        }

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex > -1 && e.RowIndex > -1)
            {
                dataGridView1.Rows[e.RowIndex].Selected = true;
                EditingMode();
            }
        }

        # region Обработчики для контекстного меню

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (point.X > -1 && point.Y > -1)
            {
                dataGridView1.ClearSelection();
                dataGridView1.Rows[point.Y].Selected = true;
                DAL.projectnumber = dataGridView1.Rows[point.Y].Cells["projectNumber"].Value.ToString();
            }
            else
                e.Cancel = true;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)   //контекстное меню - РЕДАТИРОВАТЬ
        {
            EditingMode();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)   //контекстное меню - СМЕНИТЬ НОМЕР
        {
            ChangeProjectNumber();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)   //контекстное меню - УДАЛИТЬ ПРОЕКТ
        {
            DeleteProject();
        }

        private void открытьПапкуСПроектомToolStripMenuItem_Click(object sender, EventArgs e)   //контекстное меню - открытьПапкуСПроектом
        {
            string projectFolderPath = dataGridView1.SelectedRows[0].Cells["projectFolderPath"].Value.ToString();

            if (projectFolderPath == String.Empty)
            {
                MessageBox.Show("Путь к проекту не указан",
                    "Открыть папку с проектом", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (Directory.Exists(projectFolderPath) == false)
            {
                MessageBox.Show("Такой папки не существует",
                    "Открыть папку с проектом", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                Process.Start(dataGridView1.SelectedRows[0].Cells["projectFolderPath"].Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void экспортВЧертежиИДокументыToolStripMenuItem_Click(object sender, EventArgs e) //контекстное меню - экспортВЧертежиИДокументы
        {
            DataGridViewRow drow = dataGridView1.SelectedRows[0];   //строка dataGridView1 с выбранным проектом
            string projectFolderPath = drow.Cells["projectFolderPath"].Value.ToString();  //путь к папке с проектом (может отсутствовать)

            //формируем словарь пользовательских свойств выбранного проекта
            //в чертежи и текстовые документы попадут только указанные данные из базы
            Dictionary<string, string> myDict = new Dictionary<string, string>();
            myDict.Add("Год", drow.Cells["creationYear"].Value.ToString());
            myDict.Add("Организация", drow.Cells["organization"].Value.ToString());
            myDict.Add("Номер договора", drow.Cells["numberTU"].Value.ToString()); // раньше это был "Номер ТУ(ТЗ)"                                  contractNumber
            myDict.Add("Номер ТУ(ТЗ)", drow.Cells["otherNumber"].Value.ToString()); // раньше это был "Номер иного документа"
            myDict.Add("Номер проекта", DAL.projectnumber);
            myDict.Add("Название объекта", drow.Cells["fullObjectName"].Value.ToString());
            myDict.Add("Раздел проекта", drow.Cells["projectSection"].Value.ToString());
            myDict.Add("Стадия пр работ", drow.Cells["designWorksStage"].Value.ToString());
            myDict.Add("Директор", CreateSurnameAndInitials((int)drow.Cells["id_director"].Value)); //директора указываем с инициалами
            myDict.Add("Нормоконтроль", drow.Cells["normoControl"].Value.ToString());
            myDict.Add("Инженер", drow.Cells["engineer"].Value.ToString());

            //выбираем из таблицы организацию, указанную в проекте
            var organizations = DAL.myDataSet.Tables["organizations"].AsEnumerable();
            var query1 = from p in organizations
                        where p.Field<int>("id_organization") == (int)drow.Cells["id_organization"].Value
                        select p;
            //id_organization - ключевое поле в базе, как сделать без цикла foreach не знаю
            //для указанной организации заносим в словарь её город, номер свидетельства и номер протокола СРО
            foreach (var organization in query1)
            {
                myDict.Add("Город", organization.Field<string>("city"));
                myDict.Add("СРО_номер", organization.Field<string>("SRO_number"));
                myDict.Add("СРО_протокол", organization.Field<string>("SRO_protocol"));
            }

            //выбираем из таблицы листы, относящиеся к нашему проекту
            var sheets = DAL.myDataSet.Tables["sheets"].AsEnumerable();
            var query2 = from p in sheets
                        where p.Field<string>("id_sheet") == drow.Cells["projectNumber"].Value.ToString()
                        select p;

            foreach (var sheet in query2)
            {
                myDict.Add("Лист" + sheet.Field<int>("number").ToString(), sheet.Field<string>("sheet"));
            }

            string testMessage = "Следующие данные подготовлены для экспорта: \n\n";
            foreach (KeyValuePair<string, string> kvp in myDict)
                testMessage += kvp.Key + " - \t" + kvp.Value + "\n";

            testMessage += "\nВНИМАНИЕ! Экспорт работает лучше, когда AutoCAD запущен!";

            MessageBox.Show(testMessage);


            //теперь данные для экспорта скомпанованы в словарь, можно начинать
            //если для проекта указана папка, где он лежит - openFileDialog1 открывает ее
            //иначе - откроется папка по-умолчанию
            if (projectFolderPath != String.Empty || Directory.Exists(projectFolderPath))
            {
                openFileDialog1.InitialDirectory = projectFolderPath; //стартуем из папки с нужным проектом
            }

            openFileDialog1.Filter = "Чертежи AutoCAD и документы Writer |*.odt; *.dwg";

            DialogResult result = openFileDialog1.ShowDialog();     // Show the dialog.
            if (result == DialogResult.OK)
            {
                Cursor = Cursors.WaitCursor;    //показываем режим ожидания
                this.Enabled = false;
                foreach (string filename in openFileDialog1.FileNames)  //цикл по всем файлам из диалога
                {
                    if (CanWrite(filename)) //если файл можно открыть на запись
                    {
                        if (Path.GetExtension(filename) == ".odt")
                        {
                            Export.SaveInODTdocument(myDict, filename);
                        }

                        if (Path.GetExtension(filename) == ".dwg")
                        {
							Export.SaveInAcadDrawing(myDict, filename);
                        }
                    }
                    else
                        MessageBox.Show("Не удалось получить доступ к файлу " + Path.GetFileName(filename) + "\n"
                            + "Возможно он открыт в другой программе.",
                            "Экспорт в чертежи и документы", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                this.Enabled = true;
                Cursor = Cursors.Default;   //возвращаем стандартнй курсор
            }
        }

		/// <summary>
		/// Проверить, доступен ли файл для записи
		/// </summary>
		/// <param name="path">Полный путь к файлу</param>
		/// <returns>true - файл свободен</returns>
        private bool CanWrite(string path)
        {
            try
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                fs.Dispose();
                return true;
            }
            catch
            {
                return false;
            } 
        }

        /// <summary>
		/// Создать строку из фамилии сотрудника и его инициалов
        /// </summary>
		/// <param name="id_employee">ID сотрудника из таблицы сотрудников</param>
		/// <returns>Фамилия сотрудника с инициалами, например: "А.А. Киреев"</returns>
        private string CreateSurnameAndInitials(int id_employee)
        {
            if (id_employee <= 0)
                return "";
            
            var employees = DAL.myDataSet.Tables["employees"].AsEnumerable();

            var query = from p in employees
                                  where p.Field<int>("id_employee") == id_employee
                                  select p;

            foreach (var employee in query) //будет только 1 элемент в выборке, т.к. `id_employee` - ключевое поле, а как без цикла сделать я не знаю
            {
                return
                    employee.Field<string>("name").Substring(0, 1) + "."
                    + employee.Field<string>("patronymic").Substring(0, 1) + ". "
                    + employee.Field<string>("surname");
            }
            return "";
        }

        # endregion Обработчики для контекстного меню //==================================================================================
        # region Обработчики для ToolStripMenu

        private void NewProjectToolStripMenuItem_Click(object sender, EventArgs e) //НОВЫЙ ПРОЕКТ...
        {
            NewProject();
        }

        private void ExportToCSV_ToolStripMenuItem_Click(object sender, EventArgs e)	//запускает ЭКСПОРТ ТАБЛИЦЫ В .CSV
        {
            if (dataGridView1.Rows.Count > 0)  //если dataGridView1 не пустой
            {
                saveFileDialog1.Filter = "Текстовые файлы (*.csv)|*.csv";
                saveFileDialog1.FileName = "Экспорт";
                DialogResult result = saveFileDialog1.ShowDialog();     // Show the dialog.                
                if (result == DialogResult.OK)
                {
                    Cursor = Cursors.WaitCursor;
					// =============================
					Export.ExportToCSV(saveFileDialog1.FileName, CreateExsportedTable());
					// =============================
                    Cursor = Cursors.Default;
                }
            }
            else MessageBox.Show("Нечего экспортировать.", "Экспорт в CSV", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

		private void вТаблицуOpenDocumentodsToolStripMenuItem_Click(object sender, EventArgs e)	//запускает ЭКСПОРТ ТАБЛИЦЫ В .ods
		{
			if (dataGridView1.Rows.Count > 0)  //если dataGridView1 не пустой
			{
				saveFileDialog1.Filter = "Таблицы Open Document (*.ods)|*.ods";
				saveFileDialog1.FileName = "Экспорт";
				DialogResult result = saveFileDialog1.ShowDialog();     // Show the dialog.                
				if (result == DialogResult.OK)
				{
					Cursor = Cursors.WaitCursor;
					// =============================
					Export.Export_to_ODS(saveFileDialog1.FileName, CreateExsportedTable());
					// =============================
					Cursor = Cursors.Default;
				}
			}
			else MessageBox.Show("Нечего экспортировать.", "Экспорт в ods", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private DataTable CreateExsportedTable()
		{
			DataTable table = new DataTable();	// Новая пустая таблица

			// добавим в нее видимые столбцы из dataGridView1 (все строкового типа)
			foreach (DataGridViewColumn gridColumn in dataGridView1.Columns)
			{
				if (gridColumn.Visible)
				{
					DataColumn addedColumn = new DataColumn(gridColumn.Name, typeof(string));
					addedColumn.Caption = gridColumn.HeaderText;
					table.Columns.Add(addedColumn);
				}
			}

			int countRows = dataGridView1.Rows.Count;       //число строк в dataGridView1
			int countColumns = dataGridView1.Columns.Count; //число столбцов в dataGridView1

			// Заполняем таблицу
			for (int i = 0; i < countRows; i++)     //внешний цикл по строкам
			{
				DataRow addedRow = table.NewRow();
				for (int j = 0; j < countColumns; j++)  //внутренний цикл по столбцам
				{
					if (dataGridView1.Columns[j].Visible == true)	// нужны только видимые
					{
						DataGridViewColumn gridColumn = dataGridView1.Rows[i].Cells[j].OwningColumn;

						// Если столбец типа bool - переводим 'true/false' в 'да/нет'
						if (gridColumn.ValueType.Equals(typeof(bool)))
						{
							if ((bool)dataGridView1.Rows[i].Cells[j].Value == true)
								addedRow.SetField<string>(gridColumn.Name, "да");
							else
								addedRow.SetField<string>(gridColumn.Name, "нет");
						}
						else
							// Просто записываем содержимое в виде текста
							addedRow.SetField<string>(gridColumn.Name, dataGridView1.Rows[i].Cells[j].Value.ToString());
					}
				}
				table.Rows.Add(addedRow);
			}
			return table;
		}

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)   //ВЫХОД
        {
            Application.Exit();
        }

        private void НастройкиToolStripMenuItem_Click(object sender, EventArgs e)   //НАСТРОЙКИ
        {
            Form_ViewSettings f = new Form_ViewSettings();
            f.Owner = this;
            f.ShowDialog();
        }
        # endregion Обработчики для ToolStripMenu //==================================================================================
				
    }
}
