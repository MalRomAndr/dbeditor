﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DB_editor
{
	public partial class Form2 : Form
	{
		private DataView singleDataView;    // Отображает одну выбранную строку проекта
		private BindingSource mainBS;
		private DataView sheetsDataView;	// Отображает листы для одного проекта
		private BindingSource sheetsBS;
		private Point point = new Point();  //хранит номер столбца и строки ячейки DataGridView1, находящейся в данный момент под курсором мышки

		public Form2()
		{
			InitializeComponent();
		}

		private void Form2_Load(object sender, EventArgs e)
		{
			singleDataView = new DataView(DAL.myDataSet.Tables["maindata"]);
			singleDataView.RowFilter = string.Format("[ProjectNumber] = '{0}'", DAL.projectnumber);
			mainBS = new BindingSource();
			mainBS.DataSource = singleDataView;

			//устанавливаем связи между элементами на форме и содержимым DataSet1:
			//label54.DataBindings.Add("Text", mainBS, "creationDate");
            label54.DataBindings.Add("Text", mainBS, "creationDate", true, DataSourceUpdateMode.Never, "дата не указана", "yyyy.MM.dd");
            label32.DataBindings.Add("Text", mainBS, "creationDate", true, DataSourceUpdateMode.Never, "время не указано", "T");
			label1.DataBindings.Add("Text", mainBS, "projectNumber");
			// -------------------------------------------------------------------------------------
			// Год
			for (int i = 2003; i <= System.DateTime.Now.Year + 1; i++)
			{
				comboBox4.Items.Add(i);
			}
			comboBox4.DataBindings.Add("Text", mainBS, "creationYear");
			// Организация:
			ComboBox2.DataSource = DAL.myDataSet.Tables["organizations"];
			ComboBox2.DisplayMember = "organization";
			ComboBox2.ValueMember = "id_organization";
			ComboBox2.DataBindings.Add("SelectedValue", mainBS, "id_organization");
			//
			TextBox2.DataBindings.Add("Text", mainBS, "numberTU");
			TextBox3.DataBindings.Add("Text", mainBS, "otherNumber");
			TextBox4.DataBindings.Add("Text", mainBS, "additionalNumber");
			TextBox5.DataBindings.Add("Text", mainBS, "fullObjectName");
			TextBox6.DataBindings.Add("Text", mainBS, "shortObjectName");
			// Статус
			comboBox24.DataSource = DAL.myDataSet.Tables["statuses"];
			comboBox24.DisplayMember = "status";
			comboBox24.ValueMember = "id_status";
			comboBox24.DataBindings.Add("SelectedValue", mainBS, "id_status");
			// Ярлык1
			ComboBox19.DataSource = DAL.myDataSet.Tables["labels1"];
			ComboBox19.DisplayMember = "label1";
			ComboBox19.ValueMember = "id_label1";
			ComboBox19.DataBindings.Add("SelectedValue", mainBS, "id_label1");
			// Ярлык2
			ComboBox20.DataSource = DAL.myDataSet.Tables["labels2"];
			ComboBox20.DisplayMember = "label2";
			ComboBox20.ValueMember = "id_label2";
			ComboBox20.DataBindings.Add("SelectedValue", mainBS, "id_label2");
			//
			TextBox9.DataBindings.Add("Text", mainBS, "projectSection");
			comboBox1.DataBindings.Add("Text", mainBS, "designWorksStage");
			// Директор
			BindingSource bindingSource1 = new BindingSource();
			bindingSource1.DataSource = DAL.myDataSet.Tables["employees"];
			ComboBox21.DisplayMember = "surname";
			ComboBox21.ValueMember = "id_employee";
			ComboBox21.DataSource = bindingSource1;
			ComboBox21.DataBindings.Add("SelectedValue", mainBS, "id_Director");
			// Нормоконтроль
			BindingSource bindingSource2 = new BindingSource();
			bindingSource2.DataSource = DAL.myDataSet.Tables["employees"];
			ComboBox22.DisplayMember = "surname";
			ComboBox22.ValueMember = "id_employee";
			ComboBox22.DataSource = bindingSource2;
			ComboBox22.DataBindings.Add("SelectedValue", mainBS, "id_NormoControl");
			// Инженер
			BindingSource bindingSource3 = new BindingSource();
			bindingSource3.DataSource = DAL.myDataSet.Tables["employees"];
			ComboBox3.DisplayMember = "surname";
			ComboBox3.ValueMember = "id_employee";
			ComboBox3.DataSource = bindingSource3;
			ComboBox3.DataBindings.Add("SelectedValue", mainBS, "id_Engineer");
			// Фамилия исполнителя
			BindingSource bindingSource4 = new BindingSource();
			bindingSource4.DataSource = DAL.myDataSet.Tables["employees"];
			ComboBox23.DisplayMember = "surname";
			ComboBox23.ValueMember = "id_employee";
			ComboBox23.DataSource = bindingSource4;
			ComboBox23.DataBindings.Add("SelectedValue", mainBS, "id_Executor");
            // Сбор исходных данных
            comboBox5.DataBindings.Add("Text", mainBS, "initialDataGathering");
            // Отчет по геодезии
            comboBox6.DataBindings.Add("Text", mainBS, "geodesyTechnicalReport");
            comboBox6.Items.Add("НЕ ТРЕБУЕТСЯ (причина?)");
            // Выдача заданий геодезистам
            comboBox7.DataBindings.Add("Text", mainBS, "geodesyDeliveryTasks");
            comboBox7.Items.Add("НЕ ТРЕБУЕТСЯ (причина?)");
            // Выдача документации
            comboBox8.DataBindings.Add("Text", mainBS, "documentationDelivery");
            // Тех. отчет
            comboBox11.DataBindings.Add("Text", mainBS, "technicalReport");
            comboBox11.Items.Add("НЕ ТРЕБУЕТСЯ");
            // Допуск РосТехнадзора
            comboBox12.DataBindings.Add("Text", mainBS, "RTNadmittance");
            comboBox12.Items.Add("НЕ ТРЕБУЕТСЯ");
            // Акт выбора зем. участка
            comboBox10.DataBindings.Add("Text", mainBS, "landPlotSelection");
            comboBox10.Items.Add("НЕ ТРЕБУЕТСЯ");
            // Дата выдачи ГЕО в МРСК
            comboBox9.DataBindings.Add("Text", mainBS, "GEOissuance");
            comboBox9.Items.Add("НЕ ТРЕБУЕТСЯ");
            
            // Сombobox-ы в которые добавляем дату
            ComboBox[] comboboxes = { comboBox5, comboBox6, comboBox7, comboBox8, comboBox11, comboBox12, comboBox10, comboBox9 };
            foreach (ComboBox cbox in comboboxes)
            {
                cbox.Items.Add(DateTime.Now.Date.ToString("yyyy.MM.dd"));
            }
			// -------------------------------------------------------------------------------------			
			TextBox17.DataBindings.Add("Text", mainBS, "projectReadiness");
			dateTimePicker1.DataBindings.Add("Value", mainBS, "PerformanceDate");
            dateTimePicker2.DataBindings.Add("Value", mainBS, "closingDate");
			checkBox1.DataBindings.Add("Checked", mainBS, "mounted");
			TextBox24.DataBindings.Add("Text", mainBS, "note");
			// -------------------------------------------------------------------------------------
			// Источник данных для dataGridView1:
			sheetsDataView = new DataView(DAL.myDataSet.Tables["sheets"]);
			sheetsDataView.RowFilter = string.Format("id_sheet = '{0}'", DAL.projectnumber);
			sheetsBS = new BindingSource();
			sheetsBS.DataSource = sheetsDataView;
			dataGridView1.DataSource = sheetsBS;
			// -------------------------------------------------------------------------------------
			// Красивости для dataGridView1:
			dataGridView1.Columns["number"].HeaderText = "№";
			dataGridView1.Columns["number"].ReadOnly = true;
			dataGridView1.Columns["sheet"].HeaderText = "Название листа";
			dataGridView1.Columns["sheet"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridView1.Columns["number"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridView1.RowHeadersWidth = 20;
			dataGridView1.Columns["id_sheet"].Visible = false;
			dataGridView1.Columns["number"].Width = 30;
			// -------------------------------------------------------------------------------------
			// Получим из текстового файла пункты контекстного меню с названиями чертежей:
			string[] lines = System.IO.File.ReadAllLines(
				Path.Combine(Application.StartupPath, "Resources/Названия чертежей.txt"), Encoding.Default);

			foreach (string line in lines)	// Пробегаемся по всем элементам массива...
			{
				contextMenuStrip1.Items.Add(line.Trim());	// ...и создаем из них новые пункты контекстного меню
			}
		}

		private void btn_Save_Click(object sender, EventArgs e)
		{
			mainBS.EndEdit();
			sheetsBS.EndEdit();
			DAL.SaveInDB();		// Сохраняем все изменения в базе
			this.Close();
		}

		private void btn_Cancel_Click(object sender, EventArgs e)
		{
			mainBS.CancelEdit();	// Отменяем изменения во всех элементах от mainBS

			// Отменяем изменения в оглавлении, внесенные через dataGridView1
			sheetsBS.CancelEdit();
			DAL.myDataSet.Tables["sheets"].RejectChanges();

			this.Close();
		}

		private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			dataGridView1.Rows[e.RowIndex].Cells["id_sheet"].Value = DAL.projectnumber;
			dataGridView1.Rows[e.RowIndex].Cells["number"].Value = e.RowIndex + 1;
		}

		private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
		{
			//MessageBox.Show("Некорректный ввод данных!");
		}

		private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
		{
			point.X = e.ColumnIndex;
			point.Y = e.RowIndex;
		}

		private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (point.X > 1 && point.Y > -1)
			{
				dataGridView1.Rows[point.Y].Cells[2].Selected = true;
			}
			else
				e.Cancel = true;
		}

		private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			int selectedRowNumber = dataGridView1.SelectedCells[0].RowIndex;	// Номер выделенной строки dataGridView1

			var table = DAL.myDataSet.Tables["sheets"].AsEnumerable();

			// Число видимых (не помеченных на удаление) строк DataGridVeiw:
			var rowsCount = (from n in table
							 where n.RowState != DataRowState.Deleted && n.Field<string>("id_sheet") == DAL.projectnumber
							 select n).Count();

			if (selectedRowNumber > rowsCount)
			{
				MessageBox.Show("ОШИБКА! номер выделенной строки больше, чем число строк в таблице");	// Произошла неведомая хрень
				return;
			}

			if (selectedRowNumber == rowsCount)	// Если добавляем новую строку
			{
				DAL.myDataSet.Tables["sheets"].Rows.Add(DAL.projectnumber, selectedRowNumber + 1, e.ClickedItem.Text);
				dataGridView1.Rows[0].Cells[1].Selected = true;	// Чтобы убралась непонятная вторая пустая строка
			}
			else	// Если изменяем существующую
			{
				dataGridView1.SelectedCells[0].Value = e.ClickedItem.Text;
			}
		}

		/// <summary>
		/// Перенумеровать строки по-порядку в таблице с оглавлением
		/// </summary>
		private void RenumberRows()
		{
			var table = DAL.myDataSet.Tables["sheets"].AsEnumerable();	// Таблица со всеми листами

			// Выбираем из таблицы строки для нашего проекта в два этапа:
			// 1) Исключаем Deleted и Detached
			var rows0 = from n in table
						where n.RowState != DataRowState.Deleted && n.RowState != DataRowState.Detached
					   select n;

			// 2) Выбираем из оставшихся только те, которые соответствуют номеру проекта 
			var rows = from n in rows0
					   where n.Field<string>("id_sheet") == DAL.projectnumber
					   orderby n.Field<int>("number")
					   select n;

			int number = 1;	// Нумерация начинается с 1

			foreach (DataRow row in rows)	// Перенумеровываем по-порядку
			{
				row.SetField<int>("number", number);
				number++;
			}
		}

		private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
		{
			// При удалении строки - заново перенумеровываем
			RenumberRows();
		}
	}
}