﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DB_editor
{
    public static class Translate
    {
        public static Dictionary<string, string> columnsDictionary; //словарь для перевода заголовков столбцов на русский язык

        static Translate()
        {
            columnsDictionary = new Dictionary<string, string>();
            columnsDictionary.Add("creationYear", "Год");
            columnsDictionary.Add("organization", "Организация");
            columnsDictionary.Add("numberTU", "Номер договора");    // раньше это был "Номер ТУ(ТЗ)"
            columnsDictionary.Add("otherNumber", "Номер ТУ(ТЗ)");   // раньше это был "Номер иного документа"
            columnsDictionary.Add("projectNumber", "Номер проекта");
            columnsDictionary.Add("additionalNumber", "Номер дополн.");
            columnsDictionary.Add("fullObjectName", "Название объекта полное");
            columnsDictionary.Add("shortObjectName", "Название объекта короткое");
            columnsDictionary.Add("status", "Статус");
            columnsDictionary.Add("label1", "Ярлык 1");
            columnsDictionary.Add("label2", "Ярлык 2");
            columnsDictionary.Add("projectSection", "Раздел проекта");
            columnsDictionary.Add("designWorksStage", "Стадия пр. работ");
            columnsDictionary.Add("director", "Ген. директор");
            columnsDictionary.Add("normoControl", "Нормоконтроль");
            columnsDictionary.Add("engineer", "Инженер");
            columnsDictionary.Add("executor", "Фамилия исполнителя");
            columnsDictionary.Add("creationDate", "Дата создания");
            columnsDictionary.Add("projectFolderPath", "Папка проекта");
            //
            columnsDictionary.Add("initialDataGathering", "Сбор исх. данных");            
            columnsDictionary.Add("geodesyTechnicalReport", "Получена съемка");
            columnsDictionary.Add("geodesyDeliveryTasks", "Выдача заданий геодезистам");
            columnsDictionary.Add("projectReadiness", "Стадия готовности проекта");
            columnsDictionary.Add("performanceDate", "Дата выполнения проекта");
            columnsDictionary.Add("documentationDelivery", "Выдача проектной документации");
            columnsDictionary.Add("mounted", "Смонтирован");
            columnsDictionary.Add("note", "Примечание");
            //
            columnsDictionary.Add("closingDate", "Дата закрытия $$$");
            columnsDictionary.Add("RTNadmittance", "Допуск РосТехНадзора");
            columnsDictionary.Add("landPlotSelection", "Акт выбора зем. участка");
            columnsDictionary.Add("GEOissuance", "Дата выдачи ГЕО в МРСК");
            columnsDictionary.Add("technicalReport", "Тех. отчет");
        }
    }
}