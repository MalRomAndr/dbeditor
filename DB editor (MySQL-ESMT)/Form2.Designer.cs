﻿namespace DB_editor
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.ComboBox23 = new System.Windows.Forms.ComboBox();
            this.ComboBox22 = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.ComboBox21 = new System.Windows.Forms.ComboBox();
            this.ComboBox20 = new System.Windows.Forms.ComboBox();
            this.Label19 = new System.Windows.Forms.Label();
            this.ComboBox19 = new System.Windows.Forms.ComboBox();
            this.ComboBox3 = new System.Windows.Forms.ComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.TextBox4 = new System.Windows.Forms.TextBox();
            this.Label18 = new System.Windows.Forms.Label();
            this.TextBox5 = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.TextBox6 = new System.Windows.Forms.TextBox();
            this.ComboBox2 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label15 = new System.Windows.Forms.Label();
            this.TextBox9 = new System.Windows.Forms.TextBox();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.comboBox24 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.TextBox24 = new System.Windows.Forms.TextBox();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label25 = new System.Windows.Forms.Label();
            this.Label26 = new System.Windows.Forms.Label();
            this.TextBox17 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Label29 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new UIComponent.DateTimePicker();
            this.dateTimePicker1 = new UIComponent.DateTimePicker();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.Label20 = new System.Windows.Forms.Label();
            this.TabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label54 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.TabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.TabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Save
            // 
            this.btn_Save.Image = global::DB_editor.Properties.Resources._3floppy_mount_7004;
            this.btn_Save.Location = new System.Drawing.Point(325, 516);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(116, 41);
            this.btn_Save.TabIndex = 42;
            this.btn_Save.Text = "Сохранить изменения";
            this.btn_Save.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Cancel.Image = global::DB_editor.Properties.Resources.agt_action_fail1_8480;
            this.btn_Cancel.Location = new System.Drawing.Point(212, 516);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(92, 41);
            this.btn_Cancel.TabIndex = 41;
            this.btn_Cancel.Text = "Отмена";
            this.btn_Cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Controls.Add(this.tabPage3);
            this.TabControl1.Controls.Add(this.TabPage4);
            this.TabControl1.Location = new System.Drawing.Point(4, 44);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(456, 458);
            this.TabControl1.TabIndex = 43;
            // 
            // TabPage1
            // 
            this.TabPage1.Controls.Add(this.comboBox4);
            this.TabPage1.Controls.Add(this.comboBox1);
            this.TabPage1.Controls.Add(this.ComboBox23);
            this.TabPage1.Controls.Add(this.ComboBox22);
            this.TabPage1.Controls.Add(this.label30);
            this.TabPage1.Controls.Add(this.ComboBox21);
            this.TabPage1.Controls.Add(this.ComboBox20);
            this.TabPage1.Controls.Add(this.Label19);
            this.TabPage1.Controls.Add(this.ComboBox19);
            this.TabPage1.Controls.Add(this.ComboBox3);
            this.TabPage1.Controls.Add(this.Label4);
            this.TabPage1.Controls.Add(this.Label21);
            this.TabPage1.Controls.Add(this.TextBox4);
            this.TabPage1.Controls.Add(this.Label18);
            this.TabPage1.Controls.Add(this.TextBox5);
            this.TabPage1.Controls.Add(this.Label8);
            this.TabPage1.Controls.Add(this.TextBox3);
            this.TabPage1.Controls.Add(this.label9);
            this.TabPage1.Controls.Add(this.Label7);
            this.TabPage1.Controls.Add(this.Label17);
            this.TabPage1.Controls.Add(this.TextBox6);
            this.TabPage1.Controls.Add(this.ComboBox2);
            this.TabPage1.Controls.Add(this.label6);
            this.TabPage1.Controls.Add(this.Label2);
            this.TabPage1.Controls.Add(this.Label13);
            this.TabPage1.Controls.Add(this.Label16);
            this.TabPage1.Controls.Add(this.TextBox2);
            this.TabPage1.Controls.Add(this.Label5);
            this.TabPage1.Controls.Add(this.Label15);
            this.TabPage1.Controls.Add(this.TextBox9);
            this.TabPage1.Location = new System.Drawing.Point(4, 22);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage1.Size = new System.Drawing.Size(448, 432);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "Общая информация";
            this.TabPage1.UseVisualStyleBackColor = true;
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(130, 9);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(305, 21);
            this.comboBox4.TabIndex = 51;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Р",
            "П"});
            this.comboBox1.Location = new System.Drawing.Point(130, 296);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(305, 21);
            this.comboBox1.TabIndex = 48;
            // 
            // ComboBox23
            // 
            this.ComboBox23.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ComboBox23.FormattingEnabled = true;
            this.ComboBox23.Items.AddRange(new object[] {
            "Малютин",
            "Володькин"});
            this.ComboBox23.Location = new System.Drawing.Point(130, 401);
            this.ComboBox23.Name = "ComboBox23";
            this.ComboBox23.Size = new System.Drawing.Size(305, 21);
            this.ComboBox23.TabIndex = 43;
            // 
            // ComboBox22
            // 
            this.ComboBox22.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ComboBox22.FormattingEnabled = true;
            this.ComboBox22.Items.AddRange(new object[] {
            "Малютин",
            "Володькин"});
            this.ComboBox22.Location = new System.Drawing.Point(130, 349);
            this.ComboBox22.Name = "ComboBox22";
            this.ComboBox22.Size = new System.Drawing.Size(305, 21);
            this.ComboBox22.TabIndex = 42;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(11, 326);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(78, 13);
            this.label30.TabIndex = 40;
            this.label30.Text = "Ген. директор";
            // 
            // ComboBox21
            // 
            this.ComboBox21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ComboBox21.FormattingEnabled = true;
            this.ComboBox21.Items.AddRange(new object[] {
            "Бломс Э.И."});
            this.ComboBox21.Location = new System.Drawing.Point(130, 323);
            this.ComboBox21.Name = "ComboBox21";
            this.ComboBox21.Size = new System.Drawing.Size(305, 21);
            this.ComboBox21.TabIndex = 41;
            // 
            // ComboBox20
            // 
            this.ComboBox20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ComboBox20.FormattingEnabled = true;
            this.ComboBox20.Items.AddRange(new object[] {
            "2012 - 72 объекта",
            "IV кв.-9 шт",
            "4 волна",
            "34 объекта"});
            this.ComboBox20.Location = new System.Drawing.Point(130, 248);
            this.ComboBox20.Name = "ComboBox20";
            this.ComboBox20.Size = new System.Drawing.Size(305, 21);
            this.ComboBox20.TabIndex = 39;
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label19.Location = new System.Drawing.Point(11, 379);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(53, 13);
            this.Label19.TabIndex = 29;
            this.Label19.Text = "Инженер";
            // 
            // ComboBox19
            // 
            this.ComboBox19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ComboBox19.FormattingEnabled = true;
            this.ComboBox19.Items.AddRange(new object[] {
            "МРСК"});
            this.ComboBox19.Location = new System.Drawing.Point(130, 223);
            this.ComboBox19.Name = "ComboBox19";
            this.ComboBox19.Size = new System.Drawing.Size(305, 21);
            this.ComboBox19.TabIndex = 39;
            // 
            // ComboBox3
            // 
            this.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ComboBox3.FormattingEnabled = true;
            this.ComboBox3.Items.AddRange(new object[] {
            "Малютин",
            "Володькин"});
            this.ComboBox3.Location = new System.Drawing.Point(130, 375);
            this.ComboBox3.Name = "ComboBox3";
            this.ComboBox3.Size = new System.Drawing.Size(305, 21);
            this.ComboBox3.TabIndex = 31;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label4.Location = new System.Drawing.Point(11, 15);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(25, 13);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "Год";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label21.Location = new System.Drawing.Point(11, 404);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(98, 13);
            this.Label21.TabIndex = 31;
            this.Label21.Text = "Фамилия исполн.";
            // 
            // TextBox4
            // 
            this.TextBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox4.Location = new System.Drawing.Point(130, 111);
            this.TextBox4.MaxLength = 255;
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Size = new System.Drawing.Size(305, 20);
            this.TextBox4.TabIndex = 29;
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label18.Location = new System.Drawing.Point(11, 354);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(88, 13);
            this.Label18.TabIndex = 29;
            this.Label18.Text = "Нормоконтроль";
            // 
            // TextBox5
            // 
            this.TextBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox5.Location = new System.Drawing.Point(130, 136);
            this.TextBox5.MaxLength = 255;
            this.TextBox5.Multiline = true;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBox5.Size = new System.Drawing.Size(305, 54);
            this.TextBox5.TabIndex = 2;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label8.Location = new System.Drawing.Point(11, 89);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(79, 13);
            this.Label8.TabIndex = 12;
            this.Label8.Text = "Номер ТУ(ТЗ)";
            // 
            // TextBox3
            // 
            this.TextBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox3.Location = new System.Drawing.Point(130, 86);
            this.TextBox3.MaxLength = 255;
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Size = new System.Drawing.Size(305, 20);
            this.TextBox3.TabIndex = 27;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(11, 139);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 26);
            this.label9.TabIndex = 11;
            this.label9.Text = "Название проекта\r\nполное";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label7.Location = new System.Drawing.Point(11, 114);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(109, 13);
            this.Label7.TabIndex = 28;
            this.Label7.Text = "Номер проекта доп.";
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label17.Location = new System.Drawing.Point(11, 301);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(93, 13);
            this.Label17.TabIndex = 29;
            this.Label17.Text = "Стадия пр. работ";
            // 
            // TextBox6
            // 
            this.TextBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox6.Location = new System.Drawing.Point(130, 197);
            this.TextBox6.MaxLength = 255;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Size = new System.Drawing.Size(305, 20);
            this.TextBox6.TabIndex = 28;
            // 
            // ComboBox2
            // 
            this.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ComboBox2.FormattingEnabled = true;
            this.ComboBox2.Items.AddRange(new object[] {
            "ООО \"Энергостроймонтаж\"",
            "ЗАО \"ВОЛГОЭНЕРГОСЕТЬСТРОЙ\"",
            "ООО \"Энергоинвестстрой\""});
            this.ComboBox2.Location = new System.Drawing.Point(130, 36);
            this.ComboBox2.Name = "ComboBox2";
            this.ComboBox2.Size = new System.Drawing.Size(305, 21);
            this.ComboBox2.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(11, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Название короткое";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label2.Location = new System.Drawing.Point(11, 64);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(91, 13);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "Номер договора";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label13.Location = new System.Drawing.Point(11, 226);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(50, 13);
            this.Label13.TabIndex = 29;
            this.Label13.Text = "Ярлык 1";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label16.Location = new System.Drawing.Point(11, 276);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(88, 13);
            this.Label16.TabIndex = 29;
            this.Label16.Text = "Раздел проекта";
            // 
            // TextBox2
            // 
            this.TextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox2.Location = new System.Drawing.Point(130, 61);
            this.TextBox2.MaxLength = 255;
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Size = new System.Drawing.Size(305, 20);
            this.TextBox2.TabIndex = 3;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label5.Location = new System.Drawing.Point(11, 39);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(74, 13);
            this.Label5.TabIndex = 10;
            this.Label5.Text = "Организация";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label15.Location = new System.Drawing.Point(11, 251);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(50, 13);
            this.Label15.TabIndex = 29;
            this.Label15.Text = "Ярлык 2";
            // 
            // TextBox9
            // 
            this.TextBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox9.Location = new System.Drawing.Point(130, 273);
            this.TextBox9.MaxLength = 255;
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.Size = new System.Drawing.Size(305, 20);
            this.TextBox9.TabIndex = 30;
            this.TextBox9.Text = "Электроснабжение";
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.comboBox12);
            this.TabPage2.Controls.Add(this.label31);
            this.TabPage2.Controls.Add(this.comboBox11);
            this.TabPage2.Controls.Add(this.label28);
            this.TabPage2.Controls.Add(this.label24);
            this.TabPage2.Controls.Add(this.comboBox8);
            this.TabPage2.Controls.Add(this.comboBox24);
            this.TabPage2.Controls.Add(this.label3);
            this.TabPage2.Controls.Add(this.checkBox1);
            this.TabPage2.Controls.Add(this.TextBox24);
            this.TabPage2.Controls.Add(this.Label22);
            this.TabPage2.Controls.Add(this.Label25);
            this.TabPage2.Controls.Add(this.Label26);
            this.TabPage2.Controls.Add(this.TextBox17);
            this.TabPage2.Controls.Add(this.label10);
            this.TabPage2.Controls.Add(this.Label29);
            this.TabPage2.Controls.Add(this.comboBox5);
            this.TabPage2.Controls.Add(this.label11);
            this.TabPage2.Controls.Add(this.dateTimePicker2);
            this.TabPage2.Controls.Add(this.dateTimePicker1);
            this.TabPage2.Location = new System.Drawing.Point(4, 22);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage2.Size = new System.Drawing.Size(448, 432);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "Ход выполнения проекта";
            this.TabPage2.UseVisualStyleBackColor = true;
            // 
            // comboBox12
            // 
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Location = new System.Drawing.Point(134, 265);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(307, 21);
            this.comboBox12.TabIndex = 63;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(8, 264);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(88, 26);
            this.label31.TabIndex = 62;
            this.label31.Text = "Допуск\r\nРосТехНадзора";
            // 
            // comboBox11
            // 
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Location = new System.Drawing.Point(134, 236);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(307, 21);
            this.comboBox11.TabIndex = 61;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(8, 239);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(58, 13);
            this.label28.TabIndex = 60;
            this.label28.Text = "Тех. отчет";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(8, 213);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(112, 13);
            this.label24.TabIndex = 58;
            this.label24.Text = "Дата закрытия   $$$";
            // 
            // comboBox8
            // 
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(134, 181);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(307, 21);
            this.comboBox8.TabIndex = 56;
            // 
            // comboBox24
            // 
            this.comboBox24.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox24.FormattingEnabled = true;
            this.comboBox24.Items.AddRange(new object[] {
            "МРСК"});
            this.comboBox24.Location = new System.Drawing.Point(134, 13);
            this.comboBox24.Name = "comboBox24";
            this.comboBox24.Size = new System.Drawing.Size(307, 21);
            this.comboBox24.TabIndex = 52;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(8, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "Статус";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(134, 301);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 30;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // TextBox24
            // 
            this.TextBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox24.Location = new System.Drawing.Point(134, 326);
            this.TextBox24.MaxLength = 3000;
            this.TextBox24.Multiline = true;
            this.TextBox24.Name = "TextBox24";
            this.TextBox24.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBox24.Size = new System.Drawing.Size(307, 95);
            this.TextBox24.TabIndex = 28;
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label22.ForeColor = System.Drawing.Color.Blue;
            this.Label22.Location = new System.Drawing.Point(8, 69);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(103, 26);
            this.Label22.TabIndex = 12;
            this.Label22.Text = "Стадия готовности\r\nпроекта";
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label25.Location = new System.Drawing.Point(8, 327);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(70, 13);
            this.Label25.TabIndex = 29;
            this.Label25.Text = "Примечания";
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label26.Location = new System.Drawing.Point(8, 302);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(75, 13);
            this.Label26.TabIndex = 29;
            this.Label26.Text = "Смонтирован";
            // 
            // TextBox17
            // 
            this.TextBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox17.Location = new System.Drawing.Point(134, 66);
            this.TextBox17.MaxLength = 255;
            this.TextBox17.Multiline = true;
            this.TextBox17.Name = "TextBox17";
            this.TextBox17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBox17.Size = new System.Drawing.Size(307, 82);
            this.TextBox17.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(8, 159);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(103, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "Дата вып. проекта";
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label29.Location = new System.Drawing.Point(8, 181);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(101, 26);
            this.Label29.TabIndex = 29;
            this.Label29.Text = "Выдача проектной\r\nдокументации";
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(134, 39);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(307, 21);
            this.comboBox5.TabIndex = 53;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(8, 43);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Сбор исх данных";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(134, 209);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(138, 20);
            this.dateTimePicker2.TabIndex = 59;
            this.dateTimePicker2.Value = System.DateTime.Now.Date;// new System.DateTime(2013, 12, 20, 15, 17, 11, 412);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(134, 155);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(138, 20);
            this.dateTimePicker1.TabIndex = 57;
            this.dateTimePicker1.Value = System.DateTime.Now.Date;// new System.DateTime(2013, 12, 20, 15, 17, 11, 412);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.comboBox10);
            this.tabPage3.Controls.Add(this.label23);
            this.tabPage3.Controls.Add(this.comboBox9);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.comboBox7);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.comboBox6);
            this.tabPage3.Controls.Add(this.Label20);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(448, 432);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "ГЕО";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // comboBox10
            // 
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Location = new System.Drawing.Point(134, 81);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(307, 21);
            this.comboBox10.TabIndex = 63;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(8, 79);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(72, 26);
            this.label23.TabIndex = 62;
            this.label23.Text = "Акт выбора\r\nзем. участка";
            // 
            // comboBox9
            // 
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Location = new System.Drawing.Point(134, 116);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(307, 21);
            this.comboBox9.TabIndex = 61;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(8, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(97, 26);
            this.label14.TabIndex = 60;
            this.label14.Text = "Дата выдачи ГЕО\r\nв МРСК";
            // 
            // comboBox7
            // 
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(134, 13);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(307, 21);
            this.comboBox7.TabIndex = 59;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(8, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 26);
            this.label12.TabIndex = 56;
            this.label12.Text = "Выдача заданий\r\nгеодезистам";
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(134, 47);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(307, 21);
            this.comboBox6.TabIndex = 58;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label20.Location = new System.Drawing.Point(8, 51);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(97, 13);
            this.Label20.TabIndex = 57;
            this.Label20.Text = "Получена съемка";
            // 
            // TabPage4
            // 
            this.TabPage4.Controls.Add(this.dataGridView1);
            this.TabPage4.Location = new System.Drawing.Point(4, 22);
            this.TabPage4.Name = "TabPage4";
            this.TabPage4.Size = new System.Drawing.Size(448, 432);
            this.TabPage4.TabIndex = 2;
            this.TabPage4.Text = "Содержание";
            this.TabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(448, 432);
            this.dataGridView1.TabIndex = 41;
            this.dataGridView1.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridView1_CellBeginEdit);
            this.dataGridView1.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseEnter);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            this.dataGridView1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView1_RowsRemoved);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(36, 4);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            this.contextMenuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(363, 6);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(41, 13);
            this.label54.TabIndex = 45;
            this.label54.Text = "label54";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(4, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 24);
            this.label1.TabIndex = 44;
            this.label1.Text = "Выберите проект";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(314, 6);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 13);
            this.label27.TabIndex = 46;
            this.label27.Text = "Создан:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(363, 22);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 13);
            this.label32.TabIndex = 47;
            this.label32.Text = "label32";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_Cancel;
            this.ClientSize = new System.Drawing.Size(461, 569);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.TabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Карточка проекта";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.TabPage1.PerformLayout();
            this.TabPage2.ResumeLayout(false);
            this.TabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.TabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Cancel;
        internal System.Windows.Forms.TabControl TabControl1;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TabPage TabPage2;
        internal System.Windows.Forms.TextBox TextBox24;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.TextBox TextBox17;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.TabPage TabPage4;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label27;
        internal System.Windows.Forms.ComboBox ComboBox23;
        internal System.Windows.Forms.ComboBox ComboBox22;
        internal System.Windows.Forms.Label label30;
        internal System.Windows.Forms.ComboBox ComboBox21;
        internal System.Windows.Forms.ComboBox ComboBox20;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.ComboBox ComboBox19;
        internal System.Windows.Forms.ComboBox ComboBox3;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.TextBox TextBox4;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.TextBox TextBox5;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.TextBox TextBox6;
        internal System.Windows.Forms.ComboBox ComboBox2;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.TextBox TextBox9;
        internal System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        internal System.Windows.Forms.ComboBox comboBox4;
        internal System.Windows.Forms.ComboBox comboBox24;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ComboBox comboBox8;
        private UIComponent.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ComboBox comboBox5;
        internal System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox7;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox6;
        internal System.Windows.Forms.Label Label20;
        private System.Windows.Forms.ComboBox comboBox10;
        internal System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox comboBox9;
        internal System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBox11;
        internal System.Windows.Forms.Label label28;
        private UIComponent.DateTimePicker dateTimePicker2;
        internal System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox comboBox12;
        internal System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
    }
}