﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DB_editor
{
    public partial class Form_NewNumber : Form
    {
        public Form_NewNumber()
        {
            InitializeComponent();
            label3.Text = DAL.projectnumber;    //показывает старое значение номера проекта
            textBox1.Text = DAL.projectnumber;
        }

        private void button1_Click(object sender, EventArgs e)  //ОК
        {
            //если такой номер уже есть в БД или пустая строка
            if (DAL.CheckRecordInDB(textBox1.Text) == true || textBox1.Text == string.Empty)
                MessageBox.Show("Проект с таким номером уже есть в базе данных",
                    "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);            
            else
            {
                if (DAL.ReplaceProjectNumber(textBox1.Text) == true)
                {
                    DAL.projectnumber = textBox1.Text;
                    DialogResult = DialogResult.OK;
                }
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)  //Отмена
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
