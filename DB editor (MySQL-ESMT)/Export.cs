﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;
using Autodesk.AutoCAD.Interop;
using ICSharpCode.SharpZipLib.Zip;

namespace DB_editor
{
	static class Export
	{
		// ----------------------------------------------------------------------------------------------------------------------------
		static string assemblyPath = Path.GetDirectoryName(typeof(Export).Assembly.Location);	// Папка проекта
		static AcadApplication acApp;
		// ----------------------------------------------------------------------------------------------------------------------------
		// Пространства имен для работы с файлами ODF:
		private static XNamespace office = "urn:oasis:names:tc:opendocument:xmlns:office:1.0";
		private static XNamespace table = "urn:oasis:names:tc:opendocument:xmlns:table:1.0";
		private static XNamespace text = "urn:oasis:names:tc:opendocument:xmlns:text:1.0";
		private static XNamespace meta = "urn:oasis:names:tc:opendocument:xmlns:meta:1.0";
		// ----------------------------------------------------------------------------------------------------------------------------

		# region Экспорт пользовательских данных в чертежи и документы
		/// <summary>
		/// Записать данные в файл .odt
		/// </summary>
		/// <param name="dict">Словарь с пользовательскими данными</param>
		/// <param name="fullFileName">Путь к файлу</param>
		public static void SaveInODTdocument(Dictionary<string, string> dict, string fullFileName)
		{
            if (dict.Count == 0)
                return;

			string tempFolder = Path.Combine(Application.StartupPath, "temp");  //папка "temp" для временных файлов в каталоге приложения

            DeleteTempFolder(tempFolder); // Очищаем временную папку

			FastZip zip = new FastZip();
            zip.ExtractZip(fullFileName, tempFolder, null);  //распаковываем во временную папку всё содержимое архива .odt
			XElement doc = XElement.Load(Path.Combine(tempFolder, "meta.xml")); //будем править только файл "meta.xml"

			doc.Element(office + "meta").Elements(meta + "user-defined").Remove();  //удаляем все пользовательские поля из документа

			foreach (KeyValuePair<string, string> kvp in dict)
			{
				doc.Element(office + "meta").Add(new XElement(meta + "user-defined", kvp.Value, new XAttribute(meta + "name", kvp.Key)));
			}
			doc.Save(Path.Combine(tempFolder, "meta.xml"));
            zip.CreateZip(fullFileName, tempFolder, true, null);    // Пакуем обратно
            DeleteTempFolder(tempFolder);                           // Очищаем временную папку
		}

        /// <summary>
        /// Удалить папку с временными файлами
        /// </summary>
        /// <param name="folder">Путь к папке</param>
        private static void DeleteTempFolder(string folder)
        {
            if (Directory.Exists(folder))
            {
                DirectoryInfo di = new DirectoryInfo(folder);
                di.Delete(true);
            }
        }

		/// <summary>
		/// Записать данные в чертеж AutoCAD
		/// </summary>
		/// <param name="dict">Словарь с пользовательскими данными</param>
		/// <param name="filePath">Путь к файлу</param>
		public static void SaveInAcadDrawing(Dictionary<string, string> dict, string filePath)
		{
			acApp = null;
            string progId = Properties.Settings.Default.acadVersion; //AutoCAD.Application.18.1 - AutoCAD 2011
			// Получение запущенного экземпляра приложения AutoCAD
			try
			{
				acApp = (AcadApplication)Marshal.GetActiveObject(progId);
			}
			catch // Можем получить ошибку, если искомый экземпляр AcadApplication не запущен
			{
				try
				{
					// Создать новый экземпляр AutoCAD
					acApp = (AcadApplication)Activator.CreateInstance(Type.GetTypeFromProgID(progId), true);
					acApp.Visible = false;
				}
				catch (Exception)
				{
					// Connect to our process using COM
					// We're going to loop infinitely until we get the
					// AutoCAD object.         
					// A little risky, unless we implement a timeout
					// mechanism or let the user cancel
					while (acApp == null)
					{
						try
						{
							acApp =
								(AcadApplication)Marshal.GetActiveObject(progId);
						}
						catch
						{
							// Let's let the application check its message
							// loop, in case the user has exited or cancelled
							Application.DoEvents();
						}
					}
					WaitIfBusy();
					//acAppComObj.Visible = false;
				}
			}

			// Получить ссылку на текущий документ
			AcadDocument acadDoc = null;
			while (acadDoc == null)
			{
				try
				{
					WaitIfBusy();
					acadDoc = acApp.Documents.Open(filePath);
				}
				catch
				{
					Application.DoEvents();
				}
			}

			foreach (KeyValuePair<string, string> kvp in dict)  //цикл по всем вносимым изменениям
			{
				try  //пробуем добавить запись
				{
					WaitIfBusy();
					acadDoc.SummaryInfo.AddCustomInfo(kvp.Key, kvp.Value);
				}
				catch //можем получить ошибку при попытке добавить запись с таким же ключом
				{
					bool exit = false;
					while (exit == false)
					{
						try  //пробуем
						{
							WaitIfBusy();
							acadDoc.SummaryInfo.SetCustomByKey(kvp.Key, kvp.Value); //изменяем существующую
							exit = true;
						}
						catch // если вызов отклонен
						{
							Application.DoEvents();
						}
					}
				}
			}

			WaitIfBusy();
			acadDoc.Save();     //сохраняем чертеж
			WaitIfBusy();
			acadDoc.Close();    //закрываем чертеж
			acApp.Visible = true;
		}

		private static void WaitIfBusy()
		{
			bool acadIsQuiescent = false; // статус простоя AutoCAD (false - занят, true - простаивает)

			while (acadIsQuiescent == false)	// Крутим пока не освободится
			{
				try
				{
					acadIsQuiescent = acApp.GetAcadState().IsQuiescent;	// Пробуем получить статус
					if (acadIsQuiescent == false)
						Thread.Sleep(100);	// Ждем, если занят
				}
				catch
				{
					Application.DoEvents();
					Thread.Sleep(100);	// Ждем, если не удалось получить статус
				}
			}
		}
		# endregion


		# region Экспорт выборки из БД в табличные документы

		/// <summary>
		/// Сохранить таблицу в файл .csv (простая таблица без форматирования)
		/// </summary>
		/// <param name="filePath">Полный путь к файлу</param>
		/// <param name="exsportDataTable">Экспортируемая таблица</param>
		public static void ExportToCSV(string filePath, DataTable exsportDataTable)    //экспорт таблицы в CSV
		{
			try
			{
				StreamWriter file = new StreamWriter(filePath, false, Encoding.GetEncoding(1251));
				int countRows = exsportDataTable.Rows.Count;       //число строк
				int countColumns = exsportDataTable.Columns.Count; //число столбцов

				//Заполняем заголовок таблицы
				for (int j = 0; j < countColumns; j++)
				{
					string str = @"""" + exsportDataTable.Columns[j].Caption + @"""" + ";";
					file.Write(str);
				}
				file.WriteLine();

				//Заполняем саму таблицу
				for (int i = 0; i < countRows; i++)     //внешний цикл по строкам
				{
					for (int j = 0; j < countColumns; j++)  //внутренний цикл по столбцам
					{
						string one = String.Concat('"');		// одна двойная карычка
						string two = String.Concat('"', '"');	// две двойных кавычки

						string str = exsportDataTable.Rows[i].ItemArray[j].ToString();
						str = str.Replace(one, two);	// заменяем одну двойную кавычку на 2, чтобы в CSV читалось
						str = String.Concat(one, str, one, ";");	// обрамляем строку в двойные кавычки + ';' в конце
						file.Write(str);
					}
					file.WriteLine();
				}
				file.Close();
				MessageBox.Show("Готово", "Экспорт в CSV", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Экспорт в CSV", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		/// <summary>
		/// Сохранить таблицу в текстовый файл Calc (.ods)
		/// </summary>
		/// <param name="filePath">Полный путь к файлу</param>
		/// /// <param name="exsportDataTable">Экспортируемая таблица</param>
		public static void Export_to_ODS(string filePath, DataTable exsportDataTable)
		{
			string zipName = Path.Combine(assemblyPath, "Resources/Пустой отчет.ods");

			try
			{
				// Удаляем папку с временными файлами
				if (Directory.Exists(Path.Combine(assemblyPath, "temp")))
				{
					DirectoryInfo di = new DirectoryInfo(Path.Combine(assemblyPath, "temp"));
					di.Delete(true);
				}

				//распаковываем заготовку спецификации в /temp:
				FastZip zip = new FastZip();
				zip.ExtractZip(zipName, Path.Combine(assemblyPath, "temp"), null);
				//-----------------------------------------------------------------------------------------------
				XElement doc = XElement.Load(Path.Combine(assemblyPath, "temp/content.xml"));   //содержимое документа
				XElement sheet1 = (   //получаем ссылку на Лист1
					from
						i in doc.Descendants(table + "table")
					where
						(string)i.Attribute(table + "name").Value == "Лист1"
					select i).First();
				//-----------------------------------------------------------------------------------------------			
				// Производим изменения:

				// Добавляем заголовки таблицы:
				sheet1.Add(
					newHeaderRow(exsportDataTable));

				// Добавляем само содержимое таблицы:
				for (int i = 0; i < exsportDataTable.Rows.Count; i++)
				{
					sheet1.Add(
						newRow(exsportDataTable.Rows[i]));
				}
				//-----------------------------------------------------------------------------------------------
				// Сохраняем и архивируем обратно
				doc.Save(Path.Combine(assemblyPath, "temp/content.xml"));
				zip.CreateZip(filePath, Path.Combine(assemblyPath, "temp"), true, null);

				// Удаляем папку с временными файлами
				if (Directory.Exists(Path.Combine(assemblyPath, "temp")))
				{
					DirectoryInfo di = new DirectoryInfo(Path.Combine(assemblyPath, "temp"));
					di.Delete(true);
				}
				MessageBox.Show("Готово", "Экспорт в ods", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Экспорт в ods", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private static XElement newRow(DataRow row)
		{
			XElement xRow = new XElement(table + "table-row",
									   new XAttribute(table + "style-name", "ro1"));

			for (int i = 0; i < row.ItemArray.Length; i++)
			{
				xRow.Add(
					new XElement(table + "table-cell",
						 new XAttribute(office + "value-type", "string"),
							 new XElement(text + "p", row.ItemArray[i].ToString())));
			}
			return xRow;
		}

		private static XElement newHeaderRow(DataTable myTable)
		{
			XElement xRow = new XElement(table + "table-row",
									   new XAttribute(table + "style-name", "ro1"));

			for (int i = 0; i < myTable.Columns.Count; i++)
			{
				xRow.Add(
					new XElement(table + "table-cell",
						 new XAttribute(office + "value-type", "string"),
							 new XElement(text + "p", myTable.Columns[i].Caption)));
			}
			Console.WriteLine(xRow);
			Console.ReadLine();
			return xRow;
		}
		# endregion
	}
}
