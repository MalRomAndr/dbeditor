﻿using System;
using System.Windows.Forms;

namespace DB_editor
{
    public partial class Form_ViewSettings : Form
    {
        Form1 mainForm;

        public Form_ViewSettings()
        {
            InitializeComponent();
        }

        private void Form_ViewSettings_Load(object sender, EventArgs e)
        {
            mainForm = this.Owner as Form1;
            if (mainForm == null)
            {
                this.Dispose();
            }

            GroupBox[] grpbxArray = new GroupBox[] { groupBox1, groupBox2, groupBox3 };

            foreach (GroupBox grpbx in grpbxArray)  //для каждого groupBox
            {
                foreach (Control cnt in grpbx.Controls) //перебираем все CheckBox-ы
                {
                    CheckBox cb = cnt as CheckBox;
                    if (cb != null)
                    {   //проставляем флажки согласно видимости столбцов dataGridView1
                        cb.Checked = mainForm.dataGridView1.Columns[cb.Tag.ToString()].Visible;
                    }
                }
            }
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            mainForm.dataGridView1.Columns[cb.Tag.ToString()].Visible = cb.Checked;
        }
    }
}
