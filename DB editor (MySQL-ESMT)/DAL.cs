﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;

namespace DB_editor
{
    public static class DAL
    {
        //==============================================================================================================================
        public static string projectnumber = string.Empty;   //номер проекта
        public static DataSet myDataSet = new projectsDataSet();
        static MySqlDataAdapter maindataTableAdapter, sheetsTableAdapter;
        static MySqlCommandBuilder maindataCommandBuilder, sheetsCommandBuilder;
        public static BindingSource mainBS;
        //==============================================================================================================================

        public static bool LoadData()  //заполняет DataSet из БД
        {
            myDataSet.Clear();

            using (MySqlConnection conn = new MySqlConnection(Properties.Settings.Default.projectsConnectionString))
            {
                //Создаем DataAdapter-s: Прив
                maindataTableAdapter = new MySqlDataAdapter("SELECT * FROM `maindata`;", conn);
                MySqlDataAdapter employeesTableAdapter = new MySqlDataAdapter("SELECT * FROM `employees`;", conn);
                MySqlDataAdapter labels1TableAdapter = new MySqlDataAdapter("SELECT * FROM `labels1`;", conn);
                MySqlDataAdapter labels2TableAdapter = new MySqlDataAdapter("SELECT * FROM `labels2` order by weight;", conn);  // с сортировкой по весу
                MySqlDataAdapter newproject_templatesTableAdapter = new MySqlDataAdapter("SELECT * FROM `newproject_templates`;", conn);
                MySqlDataAdapter organizationsTableAdapter = new MySqlDataAdapter("SELECT * FROM `organizations`;", conn);
                sheetsTableAdapter = new MySqlDataAdapter("SELECT * FROM `sheets`;", conn);
                MySqlDataAdapter statusesTableAdapter = new MySqlDataAdapter("SELECT * FROM `statuses`;", conn);

                maindataCommandBuilder = new MySqlCommandBuilder(maindataTableAdapter);
                sheetsCommandBuilder = new MySqlCommandBuilder(sheetsTableAdapter);

                try     //Заполняем myDataSet:
                {
                    maindataTableAdapter.Fill(myDataSet, "maindata");
                    employeesTableAdapter.Fill(myDataSet, "employees");
                    labels1TableAdapter.Fill(myDataSet, "labels1");
                    labels2TableAdapter.Fill(myDataSet, "labels2");
                    newproject_templatesTableAdapter.Fill(myDataSet, "newproject_templates");//таблица с шаблонами данных для новых проектов
                    organizationsTableAdapter.Fill(myDataSet, "organizations");
                    sheetsTableAdapter.Fill(myDataSet, "sheets");
                    statusesTableAdapter.Fill(myDataSet, "statuses");

                    // добавляем столбцы, отображающие значения родительской таблицы вместо внешнего ключа
                    // столбцы с внешними ключами скрыты от пользователя на уровне DataGridView (см. код Form1.cs)
                    // похоже, то что я написал ниже - это быдлокод))
                    if (myDataSet.Tables["maindata"].Columns.Contains("organization") == false)
                    {
                        myDataSet.Tables["maindata"].Columns.Add("organization", typeof(String), "Parent(fk_organization).organization");
                        myDataSet.Tables["maindata"].Columns["organization"].SetOrdinal(2);
                    }

                    if (myDataSet.Tables["maindata"].Columns.Contains("status") == false)
                    {
                        myDataSet.Tables["maindata"].Columns.Add("status", typeof(String), "Parent(fk_Status).status");
                        myDataSet.Tables["maindata"].Columns["status"].SetOrdinal(10);
                    }

                    if (myDataSet.Tables["maindata"].Columns.Contains("label1") == false)
                    {
                        myDataSet.Tables["maindata"].Columns.Add("label1", typeof(String), "Parent(fk_Label1).label1");
                        myDataSet.Tables["maindata"].Columns["label1"].SetOrdinal(12);
                    }

                    if (myDataSet.Tables["maindata"].Columns.Contains("label2") == false)
                    {
                        myDataSet.Tables["maindata"].Columns.Add("label2", typeof(String), "Parent(fk_Label2).label2");
                        myDataSet.Tables["maindata"].Columns["label2"].SetOrdinal(14);
                    }

                    if (myDataSet.Tables["maindata"].Columns.Contains("director") == false)
                    {
                        myDataSet.Tables["maindata"].Columns.Add("director", typeof(String), "Parent(fk_Director).surname");
                        myDataSet.Tables["maindata"].Columns["director"].SetOrdinal(18);
                    }

                    if (myDataSet.Tables["maindata"].Columns.Contains("normoControl") == false)
                    {
                        myDataSet.Tables["maindata"].Columns.Add("normoControl", typeof(String), "Parent(fk_NormoControl).surname");
                        myDataSet.Tables["maindata"].Columns["normoControl"].SetOrdinal(20);
                    }

                    if (myDataSet.Tables["maindata"].Columns.Contains("engineer") == false)
                    {
                        myDataSet.Tables["maindata"].Columns.Add("engineer", typeof(String), "Parent(fk_Engineer).surname");
                        myDataSet.Tables["maindata"].Columns["engineer"].SetOrdinal(22);
                    }

                    if (myDataSet.Tables["maindata"].Columns.Contains("executor") == false)
                    {
                        myDataSet.Tables["maindata"].Columns.Add("executor", typeof(String), "Parent(fk_Executor).surname");
                        myDataSet.Tables["maindata"].Columns["executor"].SetOrdinal(24);
                    }

                    myDataSet.AcceptChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return false;
                }
            }
        }
        //==============================================================================================================================
        public static void SaveInDB()   //сохранение содержимого DataSet1 обратно в БД
        {
            sheetsTableAdapter.Update(myDataSet.Tables["sheets"]);
            maindataTableAdapter.Update(myDataSet.Tables["maindata"]);
        }
        //==============================================================================================================================
        public static void AddNewProject(string organization, string label1, string label2,
            string projectSection, string designWorksStage, string director, string normocontrol)      //добавить новый проект в БД
        {
            MySqlCommand сmd;
            using (MySqlConnection conn = new MySqlConnection(Properties.Settings.Default.projectsConnectionString))
            {
                try
                {
                    conn.Open();
                    сmd = new MySqlCommand("INSERT INTO maindata SET "
                        + "ProjectNumber = @ProjectNumber,"
                        + "creationYear = YEAR(NOW()),"
                        + "id_organization = @id_organization,"
                        + "id_label1 = @id_label1,"
                        + "id_label2 = @id_label2,"
                        + "projectSection = @projectSection,"
                        + "designWorksStage = @designWorksStage,"
                        + "id_director = @id_director,"
                        + "id_normoControl = @id_normoControl,"
                        + "creationDate = NOW();", conn);

                    //параметры:
                    сmd.Parameters.AddWithValue("@ProjectNumber", projectnumber);
                    сmd.Parameters.AddWithValue("@id_organization", organization);
                    сmd.Parameters.AddWithValue("@id_label1", label1);
                    сmd.Parameters.AddWithValue("@id_label2", label2);
                    сmd.Parameters.AddWithValue("@projectSection", projectSection);
                    сmd.Parameters.AddWithValue("@designWorksStage", designWorksStage);
                    сmd.Parameters.AddWithValue("@id_director", director);
                    сmd.Parameters.AddWithValue("@id_normoControl", normocontrol);

                    сmd.ExecuteNonQuery();

                    MessageBox.Show("Карточка нового проекта успешно создана",
                        "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        /// ============================================================================================================================
        /// <summary>
        /// Удалить проект из БД по ключевому полю projectnumber.
        /// Из таблицы `sheets` записи вручную удалять не надо, т.к. их удаляет сама MySQL через внешние ключи (on delete - cascade)
        /// </summary>
        /// ============================================================================================================================  
        public static void DeleteProject()      //удалить проект из БД
        {
            MySqlCommand сmd;
            using (MySqlConnection conn = new MySqlConnection(Properties.Settings.Default.projectsConnectionString))
            {
                try
                {
                    conn.Open();
                    сmd = new MySqlCommand(
                        string.Format("DELETE FROM `maindata` WHERE `ProjectNumber` = '{0}';", projectnumber), conn);
                    сmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        /// ============================================================================================================================
        /// <summary>
        /// Проверить, есть ли проект в БД.
        /// </summary>
        /// <param name="number">Номер проекта (ID)</param>
        /// <returns>Есть (true) или нет (false)</returns>
        /// ============================================================================================================================
        public static bool CheckRecordInDB(string number)
        {
            using (MySqlConnection conn = new MySqlConnection(Properties.Settings.Default.projectsConnectionString))
            {
                //комманда выборки из БД по номеру проекта
                MySqlCommand selCmd = new MySqlCommand(
                    string.Format("SELECT `ProjectNumber` FROM `maindata` WHERE `ProjectNumber` = '{0}';", number),
                    conn);
                try
                {
                    conn.Open();
                    MySqlDataReader dr = selCmd.ExecuteReader();

                    if (dr.HasRows) //если MySqlDataReader dr содержит строку
                    {
                        dr.Close();
                        return true;    //есть такой проект в БД, число строк не проверяю, т.к. `ProjectNumber` ключевое поле в базе
                    }
                    else
                    {
                        dr.Close();
                        return false;   //нет такого проекта в БД
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка подключения к базе данных:\n \n " + ex.Message,
                        "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw;
                }
            }
        }

        //==============================================================================================================================
        public static bool ReplaceProjectNumber(string newNumber)   //смена номера проекта
        {
            using (MySqlConnection conn = new MySqlConnection(Properties.Settings.Default.projectsConnectionString))
            {
                try
                {
                    conn.Open();

                    MySqlCommand сmd = new MySqlCommand(
                        string.Format("UPDATE `maindata` SET `ProjectNumber` = '{0}' WHERE `ProjectNumber` = '{1}';",
                        newNumber, projectnumber), conn);

                    сmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return false;
                }
            }
        }
    }
}