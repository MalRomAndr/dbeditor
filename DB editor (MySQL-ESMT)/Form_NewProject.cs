﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;

namespace DB_editor
{
    public partial class Form_NewProject : Form
    {
        public Form_NewProject()
        {
            InitializeComponent();
        }

        private void Form_NewProject_Load(object sender, EventArgs e)
        {
            TextBox1.Text = CreateNewNumberString();

            // Добавляем столбцы, отображающие значения родительской таблицы вместо внешнего ключа
            if (DAL.myDataSet.Tables["newproject_templates"].Columns.Contains("organization") == false)
                DAL.myDataSet.Tables["newproject_templates"].Columns.Add("organization", typeof(String), "Parent(organization).organization");
            if (DAL.myDataSet.Tables["newproject_templates"].Columns.Contains("label1") == false)
                DAL.myDataSet.Tables["newproject_templates"].Columns.Add("label1", typeof(String), "Parent(label1).label1");
            if (DAL.myDataSet.Tables["newproject_templates"].Columns.Contains("label2") == false)
                DAL.myDataSet.Tables["newproject_templates"].Columns.Add("label2", typeof(String), "Parent(label2).label2");
            if (DAL.myDataSet.Tables["newproject_templates"].Columns.Contains("director") == false)
                DAL.myDataSet.Tables["newproject_templates"].Columns.Add("director", typeof(String), "Parent(director).surname");
            if (DAL.myDataSet.Tables["newproject_templates"].Columns.Contains("normoControl") == false)
                DAL.myDataSet.Tables["newproject_templates"].Columns.Add("normoControl", typeof(String), "Parent(normoControl).surname");

            DAL.myDataSet.Tables["newproject_templates"].Columns["projectSection"].SetOrdinal(10);
            DAL.myDataSet.Tables["newproject_templates"].Columns["designWorksStage"].SetOrdinal(10);

            dataGridView2.RowTemplate.Height = 40;
            dataGridView2.RowsDefaultCellStyle.ForeColor = System.Drawing.Color.Blue;

            BindingSource templateBS = new BindingSource();
            templateBS.DataSource = DAL.myDataSet.Tables["newproject_templates"];
            dataGridView2.DataSource = templateBS;

            DataGridViewButtonColumn col = new DataGridViewButtonColumn();
            col.HeaderText = "";
            col.Name = "Create";
            col.Text = "Создать";
            col.UseColumnTextForButtonValue = true;
            dataGridView2.Columns.Add(col);
            
            //пробегаемся по всем столбцам dataGridView2
            foreach (DataGridViewColumn dataColumn in dataGridView2.Columns)
            {
                if (Translate.columnsDictionary.ContainsKey(dataColumn.Name)) //если такой заголовок есть в словаре
                    dataColumn.HeaderText = Translate.columnsDictionary[dataColumn.Name]; //отображаем русский текст

                if (dataColumn.Name.Contains("id_"))    //
                    dataColumn.Visible = false;         //скрываем столбец с id
            }

            dataGridView2.RowHeadersWidth = 20;
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            
            this.Width = dataGridView2.Width + 31;
			//this.Height = dataGridView2.Height + 410;

            dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(dataGridView2_CellButtonClick);     //подписываемся на событие
        }

		/// <summary>
		/// Создать новый номер проекта. Метод перебирает все созданные номра проектов и создает новый номер max+1 - следующий по порядку.
		/// </summary>
		/// <returns>Новый номер проекта</returns>
        private string CreateNewNumberString()
        {
            var query =
                from c in DAL.myDataSet.Tables["maindata"].AsEnumerable()
                where c.Field<string>("projectNumber").Contains(System.DateTime.Now.Year.ToString())
                select c;

            if (query.Count() != 0)
            {
                ArrayList arrayList = new ArrayList();
                string s1 = String.Empty;
                int position;
                int number;

                foreach (var row in query)
                {
                    s1 = row.Field<string>("ProjectNumber");                    
                    position = s1.IndexOf("П");  //3

                    if (position != -1)  //если нашлась буква П
                    {
                        bool result = Int32.TryParse(s1.Substring(0, position), out number);
                        
                        if (result)
                            arrayList.Add(number);
                    }
                }
                int max = 0;
                foreach (int i in arrayList)
                {
                    if (i > max)
                        max = i;
                }
                return String.Format("{0:D3}", max+1) + "П-" + System.DateTime.Now.Year.ToString() + "-ЭС";
            }
            else
                return "001П-" + System.DateTime.Now.Year.ToString() + "-ЭС";
        }

        private void dataGridView2_CellButtonClick(object sender, DataGridViewCellEventArgs e)
        {
            // Ignore clicks that are not on button cells. 
            if (e.RowIndex < 0 || e.ColumnIndex !=
                dataGridView2.Columns["Create"].Index) return;
            //--------------------------------------------------------------------------------

            if (DAL.CheckRecordInDB(this.TextBox1.Text.Trim()) == false)    //если номера проекта еще нет в БД
            {
                DAL.projectnumber = TextBox1.Text.Trim();   //номер проекта
                DataGridViewRow selectedRow = dataGridView2.Rows[e.RowIndex];   //строка, на которой нажали кнопку "создать"

                //Добавляем в БД новый проект
                DAL.AddNewProject(
                    selectedRow.Cells["id_organization"].Value.ToString(),
                    selectedRow.Cells["id_label1"].Value.ToString(),
                    selectedRow.Cells["id_label2"].Value.ToString(),
                    selectedRow.Cells["projectSection"].Value.ToString(),
                    selectedRow.Cells["designWorksStage"].Value.ToString(),
                    selectedRow.Cells["id_employee_director"].Value.ToString(),
                    selectedRow.Cells["id_employee_normocontrol"].Value.ToString()
                    );
                
                DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Проект с таким номером уже есть в базе данных",
                    "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
