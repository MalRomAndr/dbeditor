﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DB_editor
{
    public partial class Form_Pass : Form
    {
        public string newValue;

        public Form_Pass()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)  //ОК
        {
            if (textBox1.Text == "1234")
                DialogResult = DialogResult.OK;
            else
                MessageBox.Show("Неправильный пароль");
        }

        private void button2_Click(object sender, EventArgs e)  //Отмена
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
