﻿using System;
using MySql.Data.MySqlClient;

namespace Synchronize_esmt
{
    public static class DAL
    {
        private static MySqlConnection conn =
            new MySqlConnection("server=192.168.89.25;Uid=user;password=123456;database=projects;Charset=utf8");


        // Token: 0x06000001 RID: 1 RVA: 0x00002080 File Offset: 0x00000280
        public static string SaveInDB(string path, string number)
        {
            MySqlCommand mySqlCommand = new MySqlCommand("UPDATE `maindata` SET `projectFolderPath` = @projectFolderPath WHERE `projectNumber` = @projectNumber;", DAL.conn);
            mySqlCommand.Parameters.AddWithValue("@projectFolderPath", path);
            mySqlCommand.Parameters.AddWithValue("@projectNumber", number);
            string result;
            using (conn)
            {
                try
                {
                    conn.Open();
                    mySqlCommand.Connection = conn;
                    mySqlCommand.ExecuteNonQuery();
                    result = "Адрес проекта успешно сохранен";
                }
                catch (Exception ex)
                {
                    result = "Ошибка сохранения в базе: " + ex.Message;
                }
            }
            return result;
        }

        // Token: 0x06000002 RID: 2 RVA: 0x00002128 File Offset: 0x00000328
        public static bool CheckRecordInDB(string projectnumber)
        {
            MySqlCommand mySqlCommand = new MySqlCommand($"SELECT `projectNumber` FROM `maindata` WHERE `projectNumber` = '{projectnumber}';", conn);
            bool result = false;

            conn.Open();
            MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
            if (mySqlDataReader.HasRows)
            {
                mySqlDataReader.Close();
                result = true;
            }
            else
            {
                mySqlDataReader.Close();
                result = false;
            }

            conn.Close();

            return result;
        }
    }
}