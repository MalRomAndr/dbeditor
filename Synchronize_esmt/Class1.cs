﻿using System;
using System.IO;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;

namespace Synchronize_esmt
{
    public class Class1
    {
        private static string projectnumber = string.Empty;
        private static string programmMessageHeader = "Cинхронизация с базой данных (esmt): ";

        // Token: 0x06000004 RID: 4 RVA: 0x000021A8 File Offset: 0x000003A8
        [CommandMethod("synchronize_esmt")]
        public void Synchronize_esmt()
        {
            Document mdiActiveDocument = Application.DocumentManager.MdiActiveDocument;
            mdiActiveDocument.Database.SaveComplete += new DatabaseIOEventHandler(this.Database_SaveComplete);
        }

        // Token: 0x06000005 RID: 5 RVA: 0x000021D8 File Offset: 0x000003D8
        public void Database_SaveComplete(object senderObj, DatabaseIOEventArgs DatabaseEventArgs)
        {
            Document mdiActiveDocument = Application.DocumentManager.MdiActiveDocument;
            string directoryName = Path.GetDirectoryName(mdiActiveDocument.Name);
            Database database = mdiActiveDocument.Database;
            DatabaseSummaryInfo summaryInfo = database.SummaryInfo;
            DatabaseSummaryInfoBuilder databaseSummaryInfoBuilder = new DatabaseSummaryInfoBuilder(summaryInfo);
            Editor editor = mdiActiveDocument.Editor;
            if (directoryName == string.Empty)
            {
                return;
            }
            using (mdiActiveDocument.LockDocument())
            {
                try
                {
                    if (databaseSummaryInfoBuilder.CustomPropertyTable.Contains("Номер проекта"))
                    {
                        projectnumber = databaseSummaryInfoBuilder.CustomPropertyTable["Номер проекта"].ToString();
                        editor.WriteMessage("\n" + programmMessageHeader + "Найден номер проекта в чертеже: " + projectnumber);
                        if (DAL.CheckRecordInDB(projectnumber))
                        {
                            editor.WriteMessage("\n" + programmMessageHeader + "Найден номер проекта в БД: " + projectnumber);
                            editor.WriteMessage("\n" + programmMessageHeader + DAL.SaveInDB(this.PathConverter(directoryName), projectnumber));
                        }
                        else
                        {
                            editor.WriteMessage("\n" + programmMessageHeader + "Нет такого проекта в БД");
                        }
                    }
                    else
                    {
                        editor.WriteMessage("\n" + programmMessageHeader + "Нет номера проекта в чертеже");
                    }
                }
                catch (System.Exception ex)
                {
                    editor.WriteMessage($"\n{ex.Message}\n{ex.InnerException}\n{ex.Source}\n{ex.StackTrace}\n{ex.Data}");
                }
            }
        }

        // Token: 0x06000006 RID: 6 RVA: 0x0000232C File Offset: 0x0000052C
        private string PathConverter(string path)
        {
            string machineName = Environment.MachineName;
            if (path.StartsWith("\\"))
            {
                return path;
            }
            return "\\\\" + machineName + "\\" + path.Replace(":", "");
        }
    }
}
