﻿using System;

/// <summary>
/// Проект ConsoleAppDemo нужен только для проверки работоспособности Synchronize_esmt.
/// </summary>
namespace ConsoleAppDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            // Должен вернуть true, т.к. такой номер есть в БД:
            Console.WriteLine(Synchronize_esmt.DAL.CheckRecordInDB("003П-2021-ЭС").ToString());
            Console.ReadKey();
        }
    }
}
